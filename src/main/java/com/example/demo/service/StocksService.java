package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Stocks;
import com.example.demo.repository.StocksRepository;



@Service
public class StocksService {

		@Autowired
		private StocksRepository repository;
		
		public List<Stocks> getAllStocks() {
			return repository.getAllStocks();
		}
		
		public Stocks getStocks(int id) {
			return repository.getStockById(id);
		}

		public Stocks saveStocks(Stocks stock) {
			return repository.editStock(stock);
		}

		public Stocks newStock(Stocks stock) {
			return repository.addStock(stock);
		}

		public int deleteStock(int id) {
			return repository.deleteStock(id);
		}

}

