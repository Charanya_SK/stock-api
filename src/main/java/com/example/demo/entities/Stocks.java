package com.example.demo.entities;

public class Stocks {
	
	 private int id;
	 private String stockTicker;
	 private double price;
	 private int volume;
	 private String buyOrSell;
	 private int statusCodecity;
	 
	 public Stocks() {
		 
	 }

	 public Stocks(int id, String stockTicker, double price, int volume, String buyOrSell, int statusCodecity) {
		super();
		this.id = id;
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCodecity = statusCodecity;
	}

	public int getId() {
		return id;
	}
	 public void setId(int id) {
		this.id = id;
	}
	 public String getStockTicker() {
		return stockTicker;
	}
	 public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}
	 public double getPrice() {
		return price;
	}
	 public void setPrice(double price) {
		this.price = price;
	}
	 public int getVolume() {
		return volume;
	}
	 public void setVolume(int volume) {
		this.volume = volume;
	}
	 public String getBuyOrSell() {
		return buyOrSell;
	}
	 public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}
	 public int getStatusCodecity() {
		return statusCodecity;
	}
	 public void setStatusCodecity(int statusCodecity) {
		this.statusCodecity = statusCodecity;
	}
	 
	 

}
