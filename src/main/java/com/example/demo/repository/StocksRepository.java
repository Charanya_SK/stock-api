package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Stocks;

@Component
public interface StocksRepository 
{
	public List<Stocks> getAllStocks();
	public Stocks getStockById(int id);
	public Stocks editStock(Stocks Stock);
	public int deleteStock(int id);
	public Stocks addStock(Stocks Stock);

}
