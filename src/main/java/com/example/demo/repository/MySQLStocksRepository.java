package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Stocks;

@Repository
public class MySQLStocksRepository implements StocksRepository
{

	@Autowired
	JdbcTemplate template;
	@Override
	public List<Stocks> getAllStocks() {
		// TODO Auto-generated method stub
		String sql = "SELECT  id, stockTicker, price, volume, buyOrSell, statusCodecity FROM stocks";
		return template.query(sql, new StocksRowMapper());
	}

	@Override
	public Stocks getStockById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT  id, stockTicker, price, volume, buyOrSell, statusCodecity FROM stocks where id=?";
		return template.queryForObject(sql, new StocksRowMapper(), id);
	}

	@Override
	public Stocks editStock(Stocks Stock) {
		// TODO Auto-generated method stub
		String sql = "UPDATE stocks SET stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCodecity = ? WHERE id = ?";
		template.update(sql,Stock.getStockTicker(),Stock.getPrice(),Stock.getVolume(),Stock.getBuyOrSell(),Stock.getStatusCodecity(),Stock.getId());
		return Stock;
	}

	@Override
	public int deleteStock(int id) {
		String sql = "DELETE FROM stocks WHERE id = ?";
		template.update(sql,id);
		return id;
	}

	@Override
	public Stocks addStock(Stocks Stock) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO stocks (stockTicker,price,volume,buyOrSell,statusCodeCity)"+
				"VALUES(?,?,?,?,?)";
		template.update(sql,Stock.getStockTicker(),Stock.getPrice(),Stock.getVolume(),Stock.getBuyOrSell(),Stock.getStatusCodecity());
		return Stock;
	}
}
class StocksRowMapper implements RowMapper<Stocks>
{
		@Override
		public Stocks mapRow(ResultSet rs, int rowNum) throws SQLException 
		{
			return new Stocks(rs.getInt("id"),rs.getString("stockTicker"),rs.getDouble("price"),rs.getInt("volume"),rs.getString("buyOrSell"),rs.getInt("statusCodecity"));
			
		}
		
}

